package pl.mobiline

/**
 * Created by ali on 12.10.15.
 */
class SplitFile {

    public static splitFileByLine() {
        def start = System.currentTimeMillis()

        Integer nOfFiles = 5
        Integer nOfLines = 999
        Integer part = Math.ceil(nOfLines/nOfFiles)
        File from = new File("from.txt")

        from.withWriter { out ->
            (1..nOfLines).each {
                out.println "$it. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam lobortis urna semper diam auctor, eget iaculis sapien commodo."
            }
        }


        (1..nOfFiles).each { file ->
            File to = new File("to_${file}_file.txt")
            to.withWriter { wr ->
                from.eachLine {line, n ->
                    if (n>(file-1)*part && n<=file*part)
                        wr.println "line n#${n}: ${line}"
                    return
                }
            }

        }

//        Integer lines = 0
//        from.eachLine {
//            lines++
//        }
        println "Execution time: ${System.currentTimeMillis()-start}"

    }
}
