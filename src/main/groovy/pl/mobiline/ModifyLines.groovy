package pl.mobiline

/**
 * Created by ali on 12.10.15.
 */
class ModifyLines {

    public static skipLines() {
        def start = System.currentTimeMillis()
        File from = new File("from.txt")
        File to = new File("to.txt")

        from.withWriter { out ->
            (1..100).each {
                out.println "$it. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam lobortis urna semper diam auctor, eget iaculis sapien commodo."
            }

        }
        to.withWriter { out ->
            ("z".."a").each {
                out.println it
            }
        }

        Integer lines = 0
        from.eachLine {
            lines++
        }
        to.withWriter { wr ->
            Integer i = 0
            wr.println from.filterLine {
                i++
                if (i != 1 && i != lines)
                    it
            }
        }
        println "Execution time: ${System.currentTimeMillis()-start}"
    }
}
